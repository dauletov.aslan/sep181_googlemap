package kz.test.googlemapapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

@SuppressLint("MissingPermission")
public class MainActivity extends AppCompatActivity {

    private JSONParser jsonParser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActivityCompat.requestPermissions(
                MainActivity.this,
                new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                },
                100
        );

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        String provider = locationManager.getBestProvider(new Criteria(), true);
        Location locations = locationManager.getLastKnownLocation(provider);
        List<String> providerList = locationManager.getAllProviders();
        if (locations != null && providerList != null && providerList.size() > 0) {
            double lat = locations.getLatitude();
            double lon = locations.getLongitude();
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
                if (addresses != null && addresses.size() > 0) {
                    Log.d("Hello", addresses.get(0).getAddressLine(0));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        jsonParser = new JSONParser();
        new MapTask().execute();

        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(
                new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        googleMap.setMyLocationEnabled(true);
                        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(new LatLng(51.1283, 71.4303))
                                .zoom(15f)
                                .bearing(180)
                                .tilt(15)
                                .build();

                        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                        googleMap.animateCamera(cameraUpdate);

                        BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.location);
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(new LatLng(47.1228, 51.9609));
                        markerOptions.icon(bitmapDescriptor);
                        markerOptions.title("Near my house");

                        googleMap.addMarker(markerOptions);

                        PolylineOptions polylineOptions = new PolylineOptions();
                        polylineOptions.add(new LatLng(47.1228, 51.9609), new LatLng(47.1219, 51.9610));
                        polylineOptions.width(5f);
                        polylineOptions.color(Color.BLUE);
                        Polyline polyline = googleMap.addPolyline(polylineOptions);

                        PolygonOptions polygonOptions = new PolygonOptions();
                        polygonOptions.add(new LatLng(47.1221, 51.9600), new LatLng(47.1227, 51.9600), new LatLng(47.1224, 51.9606));
                        polygonOptions.strokeColor(Color.GREEN);
                        polygonOptions.fillColor(Color.YELLOW);
                        polygonOptions.strokeWidth(5f);
                        Polygon polygon = googleMap.addPolygon(polygonOptions);

                        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                            @Override
                            public void onMapClick(LatLng latLng) {
                                BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.location);
                                MarkerOptions markerOptions = new MarkerOptions();
                                markerOptions.position(latLng);
                                markerOptions.icon(bitmapDescriptor);

                                googleMap.addMarker(markerOptions);
                            }
                        });

                        Location location1 = new Location("");
                        location1.setLatitude(47.1228);
                        location1.setLongitude(51.9609);

                        Location location2 = new Location("");
                        location2.setLatitude(47.1219);
                        location2.setLongitude(51.9610);

                        Log.d("Hello", "Distance: " + location1.distanceTo(location2));
                    }
                }
        );
    }

    public String makeURL(double sourceLat, double sourceLon, double destLat, double destLon) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");
        urlString.append(sourceLat);
        urlString.append(",");
        urlString.append(sourceLon);
        urlString.append("&destination=");
        urlString.append(destLat);
        urlString.append(",");
        urlString.append(destLon);
        urlString.append("&sensor=false&mode=driving&alternatives=true");
        urlString.append("&key=AIzaSyBbD1BOlBvWcR9Zp5zp3J_9nRIAZI8IhRI");
        return urlString.toString();
    }

    private class MapTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            String url = makeURL(47.1219, 51.9610, 47.1124, 51.9588);
            Log.d("Hello", "URL: " + url);
            String json = jsonParser.getJSONFromUrl(url);
            return null;
        }
    }
}